# AWS Create Cognito User and Identity Pool
Tool to create a User Pool and Identity Pool

# What this script does
 - Creates a Userpool in Cognito with the name specified in the input, with a domain of the hestia-dev and todays date
 - Creates an Identity pool with the name specified in the input
 - Creates an auth role called Cognito_${identityPoolName}_Auth_Role_${dateNow} and an unauth role called Cognito_${identityPoolName}_UnAuth_Role_${dateNow}


## Setup
### Complete config.json
Fill in config.json with the your accessKeyId and secretAccessKey for the account you want to deploy (the accessKeyId and secretAccessKey are different for alcumusdev and alcumussandbox)

## Run

Run start.js with the method createPool with the name of the user pool and identity pool

e.g.

node .\start.js createPool --userPoolName=test_pool --identityPoolName=test_identity