const aws = require('aws-sdk');
aws.config.loadFromPath('./config.json');

const provider = new aws.CognitoIdentityServiceProvider();
const identityProvider = new aws.CognitoIdentity();
const iamProvider = new aws.IAM();

const dateNow = Date.now();

const createUserPool = PoolName => {
    const userPoolConfig = {
        PoolName,
        Schema: [
            {
                AttributeDataType: 'String',
                Mutable: true,
                Name: 'email',
                Required: true
            }
        ],
        Policies: {
            PasswordPolicy: {
                MinimumLength: 8,
                RequireLowercase: true,
                RequireNumbers: true,
                RequireSymbols: false,
                RequireUppercase: true
            }
        },
        AutoVerifiedAttributes: ['email'],
        VerificationMessageTemplate: {
            DefaultEmailOption: 'CONFIRM_WITH_LINK'
        }
    };
    return provider.createUserPool(userPoolConfig).promise();
};

const createUserPoolDomain = UserPoolId => {
    const userPoolDomainConfig = {
        Domain: `hestia-dev-${dateNow}`,
        UserPoolId
    };
    return provider.createUserPoolDomain(userPoolDomainConfig).promise();
};

const createUserPoolClient = UserPoolId => {
    let userPoolClientConfig = {
        ClientName: 'al-hestia',
        ExplicitAuthFlows: ['ADMIN_NO_SRP_AUTH'],
        GenerateSecret: false,
        SupportedIdentityProviders: ['COGNITO'],
        UserPoolId
    };
    return provider.createUserPoolClient(userPoolClientConfig).promise();
};

const createIdentityPool = (IdentityPoolName, UserPoolId, ClientId) => {
    let identityProviderConfig = {
        AllowUnauthenticatedIdentities: true,
        IdentityPoolName,
        CognitoIdentityProviders: [
            {
                ClientId,
                ProviderName: `cognito-idp.${aws.config.region}.amazonaws.com/${UserPoolId}`,
                ServerSideTokenCheck: false
            }
        ]
    };
    return identityProvider.createIdentityPool(identityProviderConfig).promise();
};

const createUnAuthRole = (IdentityPoolId, identityPoolName) => {
    const params = {
        RoleName: `Cognito_${identityPoolName}_UnAuth_Role_${dateNow}`,
        AssumeRolePolicyDocument: JSON.stringify({
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Principal: {
                        Federated: 'cognito-identity.amazonaws.com'
                    },
                    Action: 'sts:AssumeRoleWithWebIdentity',
                    Condition: {
                        StringEquals: {
                            'cognito-identity.amazonaws.com:aud': IdentityPoolId
                        },
                        'ForAnyValue:StringLike': {
                            'cognito-identity.amazonaws.com:amr': 'unauthenticated'
                        }
                    }
                }
            ]
        })
    };
    return iamProvider.createRole(params).promise();
};

const createAuthRole = (IdentityPoolId, identityPoolName) => {
    const params = {
        RoleName: `Cognito_${identityPoolName}_Auth_Role_${dateNow}`,
        AssumeRolePolicyDocument: JSON.stringify({
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Principal: {
                        Federated: 'cognito-identity.amazonaws.com'
                    },
                    Action: 'sts:AssumeRoleWithWebIdentity',
                    Condition: {
                        StringEquals: {
                            'cognito-identity.amazonaws.com:aud': IdentityPoolId
                        },
                        'ForAnyValue:StringLike': {
                            'cognito-identity.amazonaws.com:amr': 'authenticated'
                        }
                    }
                }
            ]
        })
    };
    return iamProvider.createRole(params).promise();
};

const putRolePolicyAuth = (RoleName, identityPoolName) => {
    const policy = {
        RoleName,
        PolicyName: `Inline_Policy_Cognito_${identityPoolName}_UnAuth_Role_${dateNow}`,
        PolicyDocument: JSON.stringify({
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Action: ['mobileanalytics:PutEvents', 'cognito-sync:*', 'cognito-identity:*'],
                    Resource: ['*']
                }
            ]
        })
    };
    iamProvider.putRolePolicy(policy).promise();
};

const putRolePolicyUnAuth = (RoleName, identityPoolName) => {
    const policy = {
        RoleName,
        PolicyName: `Inline_Policy_Cognito_${identityPoolName}_UnAuth_Role_${dateNow}`,
        PolicyDocument: JSON.stringify({
            Version: '2012-10-17',
            Statement: [
                {
                    Effect: 'Allow',
                    Action: ['mobileanalytics:PutEvents', 'cognito-sync:*'],
                    Resource: ['*']
                }
            ]
        })
    };
    iamProvider.putRolePolicy(policy).promise();
};

const setIdentityPoolRoles = (IdentityPoolId, unauthenticated, authenticated) => {
    const identityProviderRolesConfig = {
        IdentityPoolId,
        Roles: {
            unauthenticated,
            authenticated
        }
    };
    return identityProvider.setIdentityPoolRoles(identityProviderRolesConfig).promise();
};

module.exports = {
    createUserPool,
    createUserPoolDomain,
    createUserPoolClient,
    createIdentityPool,
    setIdentityPoolRoles,
    createAuthRole,
    createUnAuthRole,
    putRolePolicyAuth,
    putRolePolicyUnAuth
};
