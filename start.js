const service = require('./aws-service');

require('yargs')
    .usage('Usage: $0 <command> [options]')
    .command(
        'createPool',
        'Create a user and identity pool',
        yargs => {
            return yargs
                .option('userPoolName', {
                    demand: 'Please Specify the name of the cognito user pool'
                })
                .option('identityPoolName', {
                    demand: 'Please Specify the name of the cognito identity pool'
                });
        },
        async argv => {
            // create user pool
            const userPoolResult = await service.createUserPool(argv.userPoolName);
            const userPoolId = userPoolResult.UserPool.Id;

            // create domain
            await service.createUserPoolDomain(userPoolId);

            // create user pool client
            const userPoolClientResult = await service.createUserPoolClient(userPoolId);
            const clientId = userPoolClientResult.UserPoolClient.ClientId;

            console.log(`User Pool created with UserPoolId=${userPoolId}, ClientId=${clientId}`)
            
            // create identity pool
            const createIdentityPoolResult = await service.createIdentityPool(argv.identityPoolName, userPoolId, clientId);
            const identityPoolId = createIdentityPoolResult.IdentityPoolId

            // create auth and unauth roles
            const authRole = await service.createAuthRole(identityPoolId, argv.identityPoolName)
            const unAuthRole = await service.createUnAuthRole(identityPoolId, argv.identityPoolName)
            // add inline policies
            await service.putRolePolicyAuth(authRole.Role.RoleName, argv.identityPoolName);
            await service.putRolePolicyUnAuth(unAuthRole.Role.RoleName, argv.identityPoolName);
            console.log(`Roles created UnAuth=${unAuthRole.Role.Arn},  Auth=${authRole.Role.Arn}`)
            
            // set roles on identity pool
            await service.setIdentityPoolRoles(identityPoolId, unAuthRole.Role.Arn, authRole.Role.Arn);

            console.log(`Identity pool created IdentityPoolId=${identityPoolId}`)
        }
    )
    .demandCommand().argv;
